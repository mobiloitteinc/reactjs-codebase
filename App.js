import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import $ from 'jquery';
import Login from './components/login';
import Authentication from './components/authentication';
import Exchange from './components/exchange';
import Wallet from './components/wallet';
import { initSocket } from './services/socket';
import { getUserDetail } from './services/global';

// Managing route loggedin and loggedout user
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('isLogged') != null
      ? <Component {...props} />
      : <Redirect to='/login' />
  )} />
)

// Login route manage
const LoginRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('isLogged') != null
      ? <Redirect to='/' />
      : <Component {...props} />
  )} />
)

class App extends React.Component {
  constructor(props){
    super(props);
    initSocket();
  }

  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route exact path='/' component={Exchange} />
            <LoginRoute exact path='/login' component={Login} />
            <PrivateRoute path='/authentication' component={Authentication} />
            <PrivateRoute path='/wallet' component={Wallet} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
