import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import Loader from 'react-loader-spinner';
import { NodeAPI } from '../services/webServices';
import { userDetail } from '../services/global';

class Authentication extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_detail: userDetail,
      authQuestion: [],
      ques1: {ques:'default', ans:''},
      ques2: {ques:'default', ans:''},
      ques3: {ques:'default', ans:''},
    }
    this.authenticationClick = this.authenticationClick.bind(this);
    this.handleQuestion = this.handleQuestion.bind(this);
  }

  // Handler when particular question changes
  handleQuestion(e){
    this.setState({[e.target.name]:{ques: e.target.value, ans:''}});
    setTimeout(() => {
      this.disableOptions();
    },500);
  }

  // Handler when particular answer changes
  handleAnswer(e) {
    this.setState({[e.target.name]:{ques:this.state[e.target.name].ques, ans:e.target.value}});
  }

  // Invoke to server regarding the authentication question
  authenticationClick(e) {
    e.preventDefault();
    let tempQues=[];
    if(this.state.ques1.ques != 'default'){
        tempQues.push({"question":this.state.ques1.ques,"answer":this.state.ques1.ans});
    }
    if(this.state.ques2.ques != 'default'){
        tempQues.push({"question":this.state.ques2.ques,"answer":this.state.ques2.ans});
    }
    if(this.state.ques3.ques != 'default'){
        tempQues.push({"question":this.state.ques3.ques,"answer":this.state.ques3.ans});
    }
    if(tempQues.length == 0){
      toast.error('Please select atleast one question', {
        position: toast.POSITION.TOP_RIGHT
      });
    }else{
      let flagAns=true;
      for(var i=0;i<tempQues.length;i++){
          if(tempQues[i].answer == ''){
              flagAns=false;
              break;
          }
      }
      if(!flagAns){
          toast.error('Answer mandatory', {
            position: toast.POSITION.TOP_RIGHT
          });
      }else{
        this.setState({loading: true});
        let auth_req = {
          "user_id":this.state.user_detail.user_id,
          "auth_ques":tempQues
        }
        return NodeAPI(auth_req, "/user_auth_ques", "POST", this.state.user_detail.token)
            .then(responseJson => {
              this.setState({loading: false});
              if(responseJson.responseCode == 200) {
                this.props.history.push('/');
              } else {
                toast.error(responseJson.responseMessage, {
                  position: toast.POSITION.TOP_RIGHT
                });
              }
            })
            .catch(error => { this.setState({loading: false}); }) 
      }
    }
  }

  componentWillMount() {
    this.getQuestionApi()
  }
 
  // API to get all questions from server
  getQuestionApi() {
    let authData = {}
    return NodeAPI(authData, "/auth_ques", "GET", this.state.user_detail.token)
    .then(responseJson => {
        if(responseJson.responseCode == 200) {
          this.setState({authQuestion: responseJson.data})
        } else {
          toast.error(responseJson.responseMessage, {
            position: toast.POSITION.TOP_RIGHT
          });
        }
    })
    .catch(error => { })
  }

  render() {
    var bgImg=require('../assets/images/bg-01.jpg');
    return (
      <div className="limiter">
        {this.state.loading ? <div className="loading_outer"><div className="loading_inner"><Loader type="Circles" color="#00BFFF" height="120" width="120" /></div></div> : null}
        <ToastContainer autoClose={3000} />
        <div className="container-login100">
          <div className="wrap-login100">
            <div className="login100-form-title" style={{backgroundImage: "url("+bgImg+")"}}>
              <span className="login100-form-title-1">Authenticate Profile</span>
            </div>
            <form className="login100-form validate-form">
              <div className="wrap-input100 validate-input mb25" data-validate="Username is required">
                <span className="label-input100">Question: 1</span>
                <select className="input100 form-control" name="ques1" value={this.state.ques1.ques} onChange={this.handleQuestion}>
                  <option value="default">Select question</option>
                  {this.state.authQuestion.map((item, i) => <option value={item.ques} disabled={item.disabled} key={i}>{item.ques}</option>)}
                </select>
                <span className="focus-input100"></span>
              </div>
              <div className="wrap-input100 validate-input mb25" data-validate="Username is required">
                <span className="label-input100">Answer</span>
                <textarea className="input100 form-control" name="ques1" placeholder="Answer" value={this.state.ques1.ans} onChange={(event) => {this.handleAnswer(event);}}></textarea>
                <span className="focus-input100"></span>
              </div>
              <div className="wrap-input100 validate-input mb25" data-validate="Username is required">
                <span className="label-input100">Question: 2</span>
                <select className="input100 form-control" name="ques2" value={this.state.ques2.ques} onChange={this.handleQuestion}>
                  <option value="default">Select question</option>
                  {this.state.authQuestion.map((item, i) => <option value={item.ques} disabled={item.disabled} key={i}>{item.ques}</option>)}
                </select>
                <span className="focus-input100"></span>
              </div>
              <div className="wrap-input100 validate-input mb25" data-validate="Username is required">
                <span className="label-input100">Answer</span>
                <textarea className="input100 form-control" name="ques2" placeholder="Answer" value={this.state.ques2.ans} onChange={(event) => {this.handleAnswer(event);}}></textarea>
                <span className="focus-input100"></span>
              </div>
              <div className="wrap-input100 validate-input mb25" data-validate="Username is required">
                <span className="label-input100">Question: 3</span>
                <select className="input100 form-control" name="ques3" value={this.state.ques3.ques} onChange={this.handleQuestion}>
                  <option value="default">Select question</option>
                  {this.state.authQuestion.map((item, i) => <option value={item.ques} disabled={item.disabled} key={i}>{item.ques}</option>)}
                </select>
                <span className="focus-input100"></span>
              </div>
              <div className="wrap-input100 validate-input mb25" data-validate="Username is required">
                <span className="label-input100">Answer</span>
                <textarea className="input100 form-control" name="ques3" placeholder="Answer" value={this.state.ques3.ans} onChange={(event) => {this.handleAnswer(event);}}></textarea>
                <span className="focus-input100"></span>
              </div>
              <div className="container-login100-form-btn">
                <button className="login100-form-btn" type="submit" onClick={this.authenticationClick}>SUBMIT</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default Authentication;