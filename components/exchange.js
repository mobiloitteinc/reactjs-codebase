import React, { Component } from 'react';
import Highcharts from 'highcharts/highstock';
import Exporting from 'highcharts/modules/exporting';
import DragPanes from 'highcharts/modules/drag-panes';
import Annotations from 'highcharts/modules/annotations';
import Indicators from 'highcharts/indicators/indicators';
import BollingerBands from 'highcharts/indicators/bollinger-bands';
import MACD from 'highcharts/indicators/macd';
import PivotPoints from 'highcharts/indicators/pivot-points';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import Loader from 'react-loader-spinner';
import { NodeAPI, thirdPartyAPI } from '../services/webServices';
import Header from './header';
import $ from 'jquery';
import moment from 'moment';
import { socket, initEvent } from '../services/socket';
import { userDetail, getUserDetail } from '../services/global';
Exporting(Highcharts);
DragPanes(Highcharts);
Annotations(Highcharts);
Indicators(Highcharts);
BollingerBands(Highcharts);
MACD(Highcharts);
PivotPoints(Highcharts);

class Exchange extends Component {

  constructor(props){
    super(props);
    this.state = {
      loading:true,
      user_detail: userDetail,
      priceRegxForDot:(/^(\d+)?([.]?\d{0,8})?$/),
      amountRegxForDot:(/^(\d+)?([.]?\d{0,2})?$/),
      current_price:0,
      coinSelect:'LTC',
      indicationSelect:'sma',
      toolSelect:'Select Tools',
      user_BTC:0,
      user_sellingCoinPrice:0,
      priceHistory:{lastPrice:'', High:'', Low:'', volume:''}
    }
    this.changeHistoryTab = this.changeHistoryTab.bind(this);
    this.handleBuyFieldsValue = this.handleBuyFieldsValue.bind(this);
  }

  componentDidMount() {
    initEvent('LTC'); // Invoke to server about the coin select and user loggedin status regarding socket
    // price history socket listen
    socket.on('userData', (data) => {
      localStorage.setItem('user',JSON.stringify(data));
      getUserDetail();
      this.setState({user_BTC: data.BTC.total_price, user_sellingCoinPrice: data[this.state.coinSelect].total_price, user_detail: JSON.parse(localStorage.getItem('user'))});
    });
    // price history socket listen
    socket.on('lastPrice', (data) => {
      this.setState({priceHistory: data});
    });
    // Buy order socket listen
    socket.on('buyOrders', (data) => {
      this.scrollableFunc();
      this.setState({buyOrderTotal: data.total, buyOrderList: data.data});
    });
    // Sell order socket listen
    socket.on('sellOrders', (data) => {
      this.scrollableFunc();
      this.setState({sellOrderTotal: data.total, sellOrderList: data.data});
    });
    // Trade history socket listen
    socket.on('tradeHistory', (data) => {
      this.scrollableFunc();
      this.setState({tradeHistoryList: data});
    });
    // User open orders socket listen
    socket.on('userOpenOrders', (data) => {
      this.scrollableFunc();
      if(this.state.fillButton){
        this.setState({userOrderList: data});
      }
    });
    // User history orders socket listen
    socket.on('userHistoryOrders', (data) => {
      this.scrollableFunc();
      if(!this.state.fillButton){
        this.setState({userOrderList: data});
      }
    });

    this.setCoinValues();
  }

  scrollableFunc(){
    window.$(".scrollbar").niceScroll({cursorborder:"",cursorcolor:"#333"});
  }

  // Change whole page data as per coin change
  changeCoin(e, type){
    e.preventDefault();
    if(this.state.coinSelect != type){
      this.setState({coinSelect: type, loading: true});
      setTimeout(() => { this.setCoinValues(); },500);
      initEvent(type);
    }
  }

  // Fill values on orders when click on particular transaction
  rowClick(e, detail){
    let tempObj = this.state['bitcoinBuyValue'];
    if(detail.transaction_type == 'Buy'){
      tempObj.sell = detail.price;
      tempObj.amountSell = detail.amount_left;
      tempObj.totalSell = detail.total;
      this.setState({bitcoinBuyValue: tempObj});
    } else if(detail.transaction_type == 'Sell'){
      tempObj.buy = detail.price;
      tempObj.amountBuy = detail.amount_left;
      tempObj.totalBuy = detail.total;
      this.setState({bitcoinBuyValue: tempObj});
    }
  }

  // Tab change for open order and order history
  changeHistoryTab(e){
    e.target.name == 'open'?this.setState({fillButton: true}):this.setState({fillButton: false});
    if(this.state.user_detail != null){
      let end_point='';
      e.target.name == 'open'?end_point = '/user_open_order_list':end_point = '/user_history_order_list';
      this.setState({userOrderList: []});
      let order_req={
        user_id:this.state.user_detail.user_id,
        coin_type:this.state.coinSelect
      };
      return NodeAPI(order_req, end_point, "POST", this.state.user_detail.token)
        .then(responseJson => {
            if(responseJson.responseCode == 200) {
              this.setState({userOrderList: responseJson.data});
            } else {
              toast.error(responseJson.responseMessage, {
                position: toast.POSITION.TOP_RIGHT
              });
            }
        })
        .catch(error => { }) 
    }
  }

  // Function to manage dots limit and calculate total amount for Buy section
  handleBuyFieldsValue(e, param1, param2, param3, param4, param5){
    if (!this.state[param5].test(e.target.value)){
      let tempVal = e.target.value.toString().split('.');
      if(param5 == 'priceRegxForDot')
        e.target.value = tempVal[0]+'.'+tempVal[1].slice(0,8);
      else
        e.target.value = tempVal[0]+'.'+tempVal[1].slice(0,2);
    }
    let tempObj = this.state[param1];
    for (var key in tempObj) {
      if (tempObj.hasOwnProperty(key)) {
          if(key == param2){
            tempObj[key] = e.target.value;
            this.setState({[param1]:tempObj});
            this.state[param1][param4] = this.state[param1][param2] * this.state[param1][param3];
            break;
          }
      }
    }
  }

  // Function to manage total amount when increase or decrease price
  addMinusPrice(val, param1, param2, param3, param4, param5, type){
    val==''?val=0:null;
    type=='add' ? val = parseFloat(val) + 0.01 : val = parseFloat(val) - 0.01;
    if(val < 0) return;
    if (!this.state[param5].test(val)){
      let tempVal = val.toString().split('.');
      if(param5 == 'priceRegxForDot')
      val = tempVal[0]+'.'+tempVal[1].slice(0,8);
      else
      val = tempVal[0]+'.'+tempVal[1].slice(0,2);
    }
    let tempObj = this.state[param1];
    for (var key in tempObj) {
      if (tempObj.hasOwnProperty(key)) {
          if(key == param2){
            tempObj[key] = val;
            this.setState({[param1]:tempObj});
            this.state[param1][param4] = this.state[param1][param2] * this.state[param1][param3];
            break;
          }
      }
    }
  }

  // Buy / Sell order function
  orderUser(e, type, price, amount, total, transaction_type){
    e.preventDefault();
    let order_req = {
        "user_id":this.state.user_detail.user_id,
        "transaction_type":transaction_type,
        "transaction":{
          "amount":parseFloat(amount),
          "price":parseFloat(price),
          "total":parseFloat(total),
          "coin_type":this.state.coinSelect
        }
    };
    return NodeAPI(order_req, '/buy_coins', "POST", this.state.user_detail.token)
      .then(responseJson => {
          if(responseJson.responseCode == 200) {
            toast.success('Your order has been placed', {
              position: toast.POSITION.TOP_RIGHT
            });
            this.resetValues(type, 'bitcoinBuyValue');
          } else {
            toast.error(responseJson.responseMessage, {
              position: toast.POSITION.TOP_RIGHT
            });
          }
      })
      .catch(error => { })
  }

  // Reset values on successful order completion
  resetValues(type, field){
    Promise.all([thirdPartyAPI({}, 'https://price?fsym='+this.state.coinSelect+'&tsyms=BTC', 'GET')]).then((data) => {
      if(field == 'bitcoinBuyValue')
        this.setState({current_price: data[0]['BTC'], [field]:{buy:data[0]['BTC'], sell:data[0]['BTC'], amountBuy:'', amountSell:'', totalBuy:'', totalSell:''}});
      else
        this.setState({current_price: data[0]['BTC'], [field]:{stopBuy:'', stopSell:'', buy:data[0]['BTC'], sell:data[0]['BTC'], amountBuy:'', amountSell:'', totalBuy:'', totalSell:''}});
    });
  }

  // Handler when logout event fires
  logout() {
    this.setState({
      user_detail:null,
      user_BTC:0,
      user_sellingCoinPrice: 0, 
      userOrderList: [],
      bitcoinBuyValue: {buy:'', sell:'', amountBuy:'', amountSell:'', totalBuy:'', totalSell:''},
      bitcoinLimitValue: {stopBuy:'', stopSell:'', buy:'', sell:'', amountBuy:'', amountSell:'', totalBuy:'', totalSell:''}
    });
    initEvent(this.state.coinSelect);
  }

  // Change graph data as per indicator change
  changeIndicator(e, type){
    if(this.state.indicationSelect != type){
      this.setState({indicationSelect: type});
      setTimeout(() => {
        this.cryptoChart();
      },500);
    }
  }

  // Change graph data as per tool change
  changeTool(e, type){
    if(this.state.toolSelect != type){
      this.setState({toolSelect: type});
      setTimeout(() => {
        this.cryptoChart();
      },500);
    }
  }

  // Chart initialization
  drawChart(chartData){
      var data = chartData.Data;
      if (chartData.Response == "Success") {
      this.graphStartDate = data[0].time * 1000;
      this.state.ohlc=[];
      this.state.volume=[];
      var openHigh=[];
      for (var i = 0; i < data.length; i++) {
          if(data[i].high!=0 && data[i].low!=0 &&data[i].open!=0 &&data[i].close!=0 ){
          this.state.ohlc.push([
              data[i].time * 1000,
              data[i].high,
              data[i].low,
              data[i].open,
              data[i].close

          ]);
          this.state.volume.push([
              data[i].time * 1000, // the date
              data[i].volumeto // the volume
          ]);
          openHigh.push(data[i].open)
          this.state.highYaxis = Math.max.apply(Math, openHigh);
          this.state.lowYaxis = Math.min.apply(Math, openHigh);
          }
          if(i == data.length-1){
              this.cryptoChart();
          }
      }
      }
  }

  render() {
    return (
      <div className="wrapper">
        {this.state.loading ? <div className="loading_outer"><div className="loading_inner"><Loader type="Circles" color="#00BFFF" height="120" width="120" /></div></div> : null}
        <ToastContainer autoClose={3000} />  
        <Header logoutHandler={ this.logout } />
        <div className="exchange-wrapper" id="sortablemain">
          <div className="exchange-row draggable" id="sortable">
            <div className="col-lg-6 pLR3 draggable clearfix">
              <div className="tradeHeader">
                <div className="col-sm-6 padd0">
                  <div className="btn-group">
                    <button data-toggle="dropdown" className="btn dropdown-toggle btn-ethren">{this.state.coinSelect} / BTC<span className="caret"></span></button>
                    <ul className="dropdown-menu dropdown-ethren">
                      <li onClick={(event) => {this.changeCoin(event, 'ETH');}}><input type="radio" id="ID1" name="NAME" value="VALUE" /><label htmlFor="ID1">ETH / BTC</label></li>
                      <li onClick={(event) => {this.changeCoin(event, 'LTC');}}><input type="radio" id="ID2" name="NAME" value="VALUE" /><label htmlFor="ID2">LTC / BTC</label></li>
                    </ul>
                  </div>
                  <div className="CurrentPrice"><p>{this.state.coinSelect} / BTC  Current Price: {this.state.current_price} (+23.19%) <i className="fa fa-signal"></i></p></div>
                </div>
                <div className="col-sm-6 padd0">
                  <div className="col-xs-3 padd0">
                    <div className="ethren-box">
                      <p>Last Price</p>
                      <p className="ethren-val">{this.state.priceHistory.lastPrice}</p>
                    </div>
                  </div>
                  <div className="col-xs-3 padd0">
                    <div className="ethren-box">
                      <p>24h High</p>
                      <p className="ethren-val">{this.state.priceHistory.High}</p>
                    </div>
                  </div>
                  <div className="col-xs-3 padd0">
                    <div className="ethren-box">
                      <p>24h Low</p>
                      <p className="ethren-val">{this.state.priceHistory.Low}</p>
                    </div>
                  </div>
                  <div className="col-xs-3 padd0">
                    <div className="ethren-box">
                      <p>24h Volume</p>
                      <p className="ethren-val">{this.state.priceHistory.volume}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tradeChart">
                <div className="tradeChart-filter">
                  <div className="btn-group">
                    <button data-toggle="dropdown" className="btn dropdown-toggle btn-tradeChart-filter">{this.state.indicationSelect}<span className="caret"></span></button>
                    <ul className="dropdown-menu dropdown-tradeChart-filter" style={{minWidth:83+'px'}}>
                      <li onClick={(event) => {this.changeIndicator(event, 'sma');}}><input type="radio" id="indicator1" name="indicator" value="indicator1" /><label htmlFor="indicator1">SMA</label></li>
                      <li onClick={(event) => {this.changeIndicator(event, 'bb');}}><input type="radio" id="indicator2" name="indicator" value="indicator2" /><label htmlFor="indicator2">Bollinger Band</label></li>
                      <li onClick={(event) => {this.changeIndicator(event, 'macd');}}><input type="radio" id="indicator3" name="indicator" value="indicator3" /><label htmlFor="indicator3">MACD</label></li>
                    </ul>
                  </div>
                  <div className="btn-group">
                    <button data-toggle="dropdown" className="btn dropdown-toggle btn-tradeChart-filter">{this.state.toolSelect}<span className="caret"></span></button>
                    <ul className="dropdown-menu dropdown-tradeChart-filter" style={{minWidth:65+'px'}}>
                      <li onClick={(event) => {this.changeTool(event, 'Select Tools');}}><input type="radio" id="tool1" name="tool" value="tool1" /><label htmlFor="tool1">Select Tools</label></li>
                      <li onClick={(event) => {this.changeTool(event, 'Fibonacci');}}><input type="radio" id="tool2" name="tool" value="tool2" /><label htmlFor="tool2">Fibonacci</label></li>
                    </ul>
                  </div>
                </div>
                {/* <div className="tradeChart-box" id="tradeChart-box">
                  <img src={require('../assets/images/tradeChart.jpg')} />
                </div> */}
                <div id="container" style={{height:562+'px'}}></div>
              </div>
            </div>
            <div className="col-lg-6 padd0 draggable clearfix" >
              <div className="col-sm-6 pLR3">
                <div className="byOrder-box" id="byOrder-body" style={{overflow: 'hidden'}}>
                  <div className="tradeHeader byOrder">
                    <h5>BUY ORDERS</h5>
                    <p>Total : {this.state.buyOrderTotal} {this.state.coinSelect}</p>
                  </div>
                  <div className="byOrder-body">
                    <div className="table-responsivez">
                      <table className="table table-bordered table-scroll byOrder-table">
                        <thead>
                          <tr>
                            <th className="col3">Price (BTC)</th>
                            <th className="col3">Amount ({this.state.coinSelect})</th>
                            <th className="col3">Total (BTC)</th>
                          </tr>
                        </thead>
                        <tbody className="scrollbar" style={{height:240+'px'}}>
                          {this.state.buyOrderList.map((item, i) => 
                            <tr key={i} onClick={(event) => {this.rowClick(event, item);}}>
                              <td className="td-green col3">{item.price}</td>
                              <td className="td-green col3">{item.amount_left}</td>
                              <td className="td-green col3">{item.price * item.amount_left}</td>
                            </tr>
                          )}
                          {this.state.buyOrderList.length == 0 ? 
                            <tr>
                              <td className="sell-text" colSpan="8">No records found.</td>
                            </tr> : null
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div className="sellOrder-box" id="sellOrder-body" style={{overflow: 'hidden'}}>
                  <div className="tradeHeader sellOrder">
                    <h5>SELL ORDERS</h5>
                    <p>Total : {this.state.sellOrderTotal} {this.state.coinSelect}</p>
                  </div>
                  <div className="sellOrder-body">
                    <div className="table-responsivez">
                      <table className="table table-bordered table-scroll sellOrder-table">
                        <thead>
                          <tr>
                            <th className="col3">Price (BTC)</th>
                            <th className="col3">Amount ({this.state.coinSelect})</th>
                            <th className="col3">Total (BTC)</th>
                          </tr>
                        </thead>
                        <tbody className="scrollbar" style={{height:240+'px'}}>
                          {this.state.sellOrderList.map((item, i) => 
                            <tr key={i} onClick={(event) => {this.rowClick(event, item);}}>
                              <td className="td-pink col3">{item.price}</td>
                              <td className="td-pink col3">{item.amount_left}</td>
                              <td className="td-pink col3">{item.price * item.amount_left}</td>
                            </tr>
                          )}
                          {this.state.sellOrderList.length == 0 ? 
                            <tr>
                              <td className="sell-text" colSpan="8">No records found.</td>
                            </tr> : null
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 pLR3">
                <div className="tradeHistory-box">
                  <div className="tradeHeader tradeHistory">
                    <h5>TRADE HISTORY</h5>
                  </div>
                  <div className="tradeHistory-body" id="tradeHistory-body">
                    <div className="table-responsivez">
                      <table className="table table-bordered table-scroll tradeHistory-table">
                        <thead>
                          <tr>
                            <th className="col4">Type</th>
                            <th className="col4">Price (BTC)</th>
                            <th className="col4">Amount ({this.state.coinSelect})</th>
                            <th className="col4">Time</th>
                          </tr>
                        </thead>
                        <tbody className="scrollbar" style={{height:582+'px'}}>
                          {this.state.tradeHistoryList.map((item, i) => 
                            <tr key={i}>
                              <td className={`col4 ${item.transaction_type == 'Buy' ? 'buy-text' : 'sell-text'}`}>{item.transaction_type}</td>
                              <td className="col4">{item.price}</td>
                              <td className="col4">{item.amount}</td>
                              <td className="col4">{moment(item.created_at).format('MM/DD/YY hh:mm A')}</td>
                            </tr>
                          )}
                          {this.state.tradeHistoryList.length == 0 ? 
                            <tr>
                              <td className="sell-text" colSpan="8">No records found.</td>
                            </tr> : null
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="exchange-row draggable" id="sortable2">
            <div className="col-lg-6 pLR3 draggable clearfix">
              <div className="myOpenOrder">
                <div className="tradeChart-filter">
                  <button type="button" name="open" className={`btn btn-tradeChart-filter ${this.state.fillButton ? 'fill-block' : ''}`} onClick={this.changeHistoryTab}>My Open Orders</button>
                  <button type="button" name="history" className={`btn btn-tradeChart-filter ${!this.state.fillButton ? 'fill-block' : ''}`} onClick={this.changeHistoryTab}>My Order History</button>
                </div>
                <div className="myOpenOrder-body">
                  <div className="table-responsive">
                    <table className="table table-bordered table-scroll myOpenOrder-table">
                      <thead>
                        <tr>
                          <th>Type</th>
                          <th>Price (BTC)</th>
                          <th>Amount ({this.state.coinSelect})</th>
                          <th>Total (BTC)</th>
                          <th>Rate/Stop</th>
                          <th>Date</th>
                          { this.state.fillButton ? <th>Action</th> : null }
                        </tr>
                      </thead>
                      <tbody className="scrollbar" style={{height: 207+'px'}}>
                        {this.state.userOrderList.map((item, i) => 
                          <tr key={i}>
                            <td className="buy-text" className={`${item.transaction_type == 'Buy' ? 'buy-text' : 'sell-text'}`}>{item.transaction_type}</td>
                            <td className="td-green">{item.price}</td>
                            { this.state.fillButton ? <td className="td-green">{item.amount_left}</td> : <td className="td-green">{item.amount}</td> }
                            {/* <td className="td-green">{item.amount}</td> */}
                            { this.state.fillButton ? <td className="td-green">{item.price * item.amount_left}</td> : <td className="td-green">{item.price * item.amount}</td> }
                            {/* <td className="td-green">{item.price * item.amount}</td> */}
                            <td>{item.stop}</td>
                            <td className="td-green">{moment(item.created_at).format('MM/DD/YY hh:mm A')}</td>
                            { this.state.fillButton ? <td>{item.state}</td> : null }
                          </tr>
                        )}
                        {this.state.userOrderList.length == 0 ? 
                          <tr>
                            <td className="sell-text" colSpan="8">No records found.</td>
                          </tr> : null
                        }
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6 pLR3 draggable clearfix">
              <div className="exchange-tabbox">
                <ul className="nav nav-tabs nav-justified exchange-nav-tabs" role="tablist">
                  <li role="presentation" className="active"><a href="#buysell" role="tab" data-toggle="tab">BUY / SELL</a></li>
                  <li role="presentation"><a href="#stoplimit" role="tab" data-toggle="tab">STOP / LIMIT</a></li>
                </ul>
                <div className="tab-content">
                  <div role="tabpanel" className="tab-pane active" id="buysell"> 
                    <div className="exchange-tabbody">
                      <div className="col-sm-6 padd0">
                        <div className="exchange-tabcontent">
                          <form className="form-horizontal exchange-form">
                            <div className="form-boxes">
                              <div className="form-group">
                                <div className="col-sm-12">
                                  <strong className="title-exchange">Buy {this.state.coinSelect}</strong>
                                  <small className="exchange-title-right"><img src={require('../assets/images/icon-wallet.png')} />You have <span style={{color: '#ffbf00'}}>{this.state.user_BTC}</span> BTC</small>
                                </div>
                              </div>
                              <div className="form-group">
                                <div className="col-sm-12">
                                  <div className="field-box">
                                    <label className="label-exchange">Price(BTC):</label>
                                    <input type="number" name="price" className="form-control exchange-input" value={this.state.bitcoinBuyValue.buy} onChange={(event) => {this.handleBuyFieldsValue(event, 'bitcoinBuyValue', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot');}} />
                                    <span className="valincdec">
                                      <i className="fa fa-angle-up valup" onClick={(event) => {this.addMinusPrice(this.state.bitcoinBuyValue.buy, 'bitcoinBuyValue', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot', 'add');}}></i>
                                      <i className="fa fa-angle-down valdown" onClick={(event) => {this.addMinusPrice(this.state.bitcoinBuyValue.buy, 'bitcoinBuyValue', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot', 'minus');}}></i>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div className="form-group">
                                <div className="col-sm-12">
                                  <div className="field-box">
                                    <label className="label-exchange">Amount({this.state.coinSelect}):</label>
                                    <input type="number" name="amount" className="form-control exchange-input" value={this.state.bitcoinBuyValue.amountBuy} onChange={(event) => {this.handleBuyFieldsValue(event, 'bitcoinBuyValue', 'amountBuy', 'buy', 'totalBuy', 'amountRegxForDot');}} />
                                  </div>
                                </div>
                              </div>
                              <div className="form-group">
                                <div className="col-sm-12">
                                  <div className="field-box">
                                    <label className="label-exchange">Total(BTC):</label>
                                    <input type="text" name="total" className="form-control exchange-input" value={this.state.bitcoinBuyValue.totalBuy} onChange={(event) => {this.handleBuyFieldsValue(event, 'bitcoinBuyValue', 'totalBuy');}} readOnly />
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="form-group padd0 mb0">
                              <div className="exchange-form-footer">
                                <button className="btn btn-order btn-buy" onClick={(event) => {this.orderUser(event, 'buyBuy', this.state.bitcoinBuyValue.buy, this.state.bitcoinBuyValue.amountBuy, this.state.bitcoinBuyValue.totalBuy, 'Buy');}} disabled={this.state.user_detail == null || this.state.bitcoinBuyValue.totalBuy == 0 || this.state.bitcoinBuyValue.totalBuy>this.state.user_BTC}>Order to Buy</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div className="col-sm-6 padd0">
                        <div className="exchange-tabcontent">
                          <form className="form-horizontal exchange-form">
                            <div className="form-group">
                              <div className="col-sm-12">
                                <strong className="title-exchange">Sell {this.state.coinSelect}</strong>
                                <small className="exchange-title-right"><img src={require('../assets/images/icon-wallet.png')} />You have <span style={{color: '#ffbf00'}}>{this.state.user_sellingCoinPrice}</span> {this.state.coinSelect}</small>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Price(BTC):</label>
                                  <input type="number" name="price" className="form-control exchange-input" value={this.state.bitcoinBuyValue.sell} onChange={(event) => {this.handleBuyFieldsValue(event, 'bitcoinBuyValue', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot');}} />
                                  <span className="valincdec">
                                    <i className="fa fa-angle-up valup" onClick={(event) => {this.addMinusPrice(this.state.bitcoinBuyValue.sell, 'bitcoinBuyValue', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot', 'add');}}></i>
                                    <i className="fa fa-angle-down valdown" onClick={(event) => {this.addMinusPrice(this.state.bitcoinBuyValue.sell, 'bitcoinBuyValue', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot', 'minus');}}></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Amount({this.state.coinSelect}):</label>
                                  <input type="number" name="amount" className="form-control exchange-input" value={this.state.bitcoinBuyValue.amountSell} onChange={(event) => {this.handleBuyFieldsValue(event, 'bitcoinBuyValue', 'amountSell', 'sell', 'totalSell', 'amountRegxForDot');}} />
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Total(BTC):</label>
                                  <input type="text" name="total" className="form-control exchange-input" value={this.state.bitcoinBuyValue.totalSell} onChange={(event) => {this.handleBuyFieldsValue(event, 'bitcoinBuyValue', 'totalSell');}} readOnly />
                                </div>
                              </div>
                            </div>
                            <div className="form-group padd0 mb0">
                              <div className="exchange-form-footer">
                                <button className="btn btn-order btn-sell" onClick={(event) => {this.orderUser(event, 'buySell', this.state.bitcoinBuyValue.sell, this.state.bitcoinBuyValue.amountSell, this.state.bitcoinBuyValue.totalSell, 'Sell');}} disabled={this.state.user_detail == null || this.state.bitcoinBuyValue.totalSell == 0 || this.state.bitcoinBuyValue.totalSell>this.state.user_sellingCoinPrice}>Order to Sell</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div role="tabpanel" className="tab-pane" id="stoplimit">
                    <div className="exchange-tabbody">
                      <div className="col-sm-6 padd0">
                        <div className="exchange-tabcontent">
                          <form className="form-horizontal exchange-form">
                            <div className="form-boxes">
                            <div className="form-group">
                              <div className="col-sm-12">
                                <strong className="title-exchange">Buy {this.state.coinSelect}</strong>
                                <small className="exchange-title-right"><img src={require('../assets/images/icon-wallet.png')} />You have <span style={{color: '#ffbf00'}}>{this.state.user_BTC}</span> BTC</small>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Stop(BTC):</label>
                                  <input type="number" name="stop" className="form-control exchange-input" value={this.state.bitcoinLimitValue.stopBuy} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'stopBuy', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot');}} />
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Limit(BTC):</label>
                                  <input type="number" name="limit" className="form-control exchange-input" value={this.state.bitcoinLimitValue.buy} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'buy', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot');}} />
                                  <span className="valincdec">
                                    <i className="fa fa-angle-up valup" onClick={(event) => {this.addMinusPrice(this.state.bitcoinLimitValue.buy, 'bitcoinLimitValue', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot', 'add');}}></i>
                                    <i className="fa fa-angle-down valdown" onClick={(event) => {this.addMinusPrice(this.state.bitcoinLimitValue.buy, 'bitcoinLimitValue', 'buy', 'amountBuy', 'totalBuy', 'priceRegxForDot', 'minus');}}></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="form-group mb5">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Amount({this.state.coinSelect}):</label>
                                  <input type="number" name="amount" className="form-control exchange-input" value={this.state.bitcoinLimitValue.amountBuy} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'amountBuy', 'buy', 'amountBuy', 'totalBuy', 'amountRegxForDot');}} />
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Total(BTC):</label>
                                  <input type="number" name="total" className="form-control exchange-input" value={this.state.bitcoinLimitValue.totalBuy} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'totalBuy');}} readOnly />
                                </div>
                              </div>
                            </div>
                          </div>
                            <div className="form-group padd0 mb0">
                              <div className="exchange-form-footer stophbox-padd">
                                <button className="btn btn-order btn-buy" onClick={(event) => {this.limitUser(event, 'limitBuy', this.state.bitcoinLimitValue.stopBuy, this.state.bitcoinLimitValue.buy, this.state.bitcoinLimitValue.amountBuy, this.state.bitcoinLimitValue.totalBuy, 'Buy');}} disabled={this.state.user_detail == null || this.state.bitcoinLimitValue.totalBuy == 0 || this.state.bitcoinLimitValue.stopBuy == '' ||  this.state.bitcoinLimitValue.totalBuy>this.state.user_BTC}>Order to Buy</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div className="col-sm-6 padd0">
                        <div className="exchange-tabcontent">
                          <form className="form-horizontal exchange-form">
                            <div className="form-group">
                              <div className="col-sm-12">
                                <strong className="title-exchange">Sell {this.state.coinSelect}</strong>
                                <small className="exchange-title-right"><img src={require('../assets/images/icon-wallet.png')} />You have <span style={{color: '#ffbf00'}}>{this.state.user_sellingCoinPrice}</span> {this.state.coinSelect}</small>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Stop(BTC):</label>
                                  <input type="number" name="stop" className="form-control exchange-input" value={this.state.bitcoinLimitValue.stopSell} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'stopSell', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot');}} />
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Limit(BTC):</label>
                                  <input type="number" name="limit" className="form-control exchange-input" value={this.state.bitcoinLimitValue.sell} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'sell', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot');}} />
                                  <span className="valincdec">
                                    <i className="fa fa-angle-up valup" onClick={(event) => {this.addMinusPrice(this.state.bitcoinLimitValue.sell, 'bitcoinLimitValue', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot', 'add');}}></i>
                                    <i className="fa fa-angle-down valdown" onClick={(event) => {this.addMinusPrice(this.state.bitcoinLimitValue.sell, 'bitcoinLimitValue', 'sell', 'amountSell', 'totalSell', 'priceRegxForDot', 'minus');}}></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="form-group mb5">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Amount({this.state.coinSelect}):</label>
                                  <input type="number" name="amount" className="form-control exchange-input" value={this.state.bitcoinLimitValue.amountSell} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'amountSell', 'sell', 'amountSell', 'totalSell', 'amountRegxForDot');}} />
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="col-sm-12">
                                <div className="field-box">
                                  <label className="label-exchange">Total(BTC):</label>
                                  <input type="number" name="total" className="form-control exchange-input" value={this.state.bitcoinLimitValue.totalSell} onChange={(event) => {this.handleLimitFieldsValue(event, 'bitcoinLimitValue', 'amountSell');}} readOnly />
                                </div>
                              </div>
                            </div>
                            <div className="form-group padd0 mb0">
                              <div className="exchange-form-footer stophbox-padd">
                                <button className="btn btn-order btn-sell" onClick={(event) => {this.limitUser(event, 'limitSell', this.state.bitcoinLimitValue.stopSell, this.state.bitcoinLimitValue.sell, this.state.bitcoinLimitValue.amountSell, this.state.bitcoinLimitValue.totalSell, 'Sell');}} disabled={this.state.user_detail == null || this.state.bitcoinLimitValue.totalSell == 0 || this.state.bitcoinLimitValue.stopSell == '' || this.state.bitcoinLimitValue.totalSell>this.state.user_sellingCoinPrice}>Order to Sell</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Exchange;