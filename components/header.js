import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { NodeAPI } from '../services/webServices';
import { ToastContainer, toast } from 'react-toastify';
import { userDetail, logout } from '../services/global';
import { socket, initEvent } from '../services/socket';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loginStatus: false,
            user_detail: userDetail,
        }
    }

    componentDidMount(){
        if(localStorage.getItem('isLogged') != null){
            this.state.loginStatus = true; // Manage header for logged in and non logged in status
            this.forceUpdate();
        }
    }

    // Logout handler functionality
    logoutClk(e){
        let logout_req = {  //request for logout
            "user_id":this.state.user_detail.user_id
        }
        return NodeAPI(logout_req, '/logout', "POST", this.state.user_detail.token)
            .then(responseJson => {
                if(responseJson.responseCode == 200) {
                    logout();
                    this.setState({loginStatus: false, user_detail:null});
                    this.props.logoutHandler();
                } else {
                    toast.error(responseJson.responseMessage, {
                    position: toast.POSITION.TOP_RIGHT
                    });
                }
            })
            .catch(error => { })
    }

    render(){
        return(
            <div>
                <ToastContainer autoClose={3000} />
                {this.state.loginStatus ? 
                    <header className="exchange-header header-loged">
                        <nav className="navbar navbar-default cust-navbar">
                        <div className="container-fluid">
                            <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#mian-menu" aria-expanded="false">
                                <span className="fa fa-bars"></span>
                            </button>
                            <a href="#" className="navbar-brand">
                                <img src={require('../assets/images/logo.png')} />
                            </a>
                            </div>
                            <div className="profile-menu">
                            <ul>
                                <li><Link to={'wallet'}>Wallet</Link></li>
                                <li><Link to={''}>Exchange</Link></li>
                                <li><a >News</a></li>
                                <li><a >About Us</a></li> 
                                <li className="dropdown profile-li">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <span>Welcome, {this.state.user_detail.name}</span>
                                <img className="welcome-img" src={require('../assets/images/welcome-img.png')} /></a>
                                <ul className="dropdown-menu dropdown-menu-right">
                                    <li><a href="profile.html">View Profile</a></li>
                                    <li><a href="change-password.html">Change Password</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#logout-modal">Logout</a></li>
                                </ul>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </nav>
                        <div id="logout-modal" className="modal fade global-modal2" role="dialog" data-backdrop="">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                <div className="modal-header model-header-bdr clearfix" style={{border:'none'}}>
                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><span>×</span></button>
                                </div>
                                <div className="modal-body">
                                    <p className="modal-msg">Are you sure you want to Logout ?</p>
                                    <div className="form-action-btn">
                                    <a className="btn btn-light-green" data-dismiss="modal" onClick={(event) => {this.logoutClk(event);}} >Yes</a>
                                    <button className="btn btn-red-bg" data-dismiss="modal" aria-hidden="true">No</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </header> : 
                    <header className="exchange-header">
                        <nav className="navbar navbar-default cust-navbar">
                        <div className="container-fluid">
                            <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#mian-menu" aria-expanded="false">
                                <span className="fa fa-bars"></span>
                            </button>
                            <a href="#" className="navbar-brand">
                                <img src={require('../assets/images/logo.png')} />
                            </a>
                            </div>
                            <div className="collapse navbar-collapse" id="mian-menu">
                                <ul className="nav navbar-nav cust-navbar-nav navbar-right">
                                    <li><Link to={''}>Exchange</Link></li>
                                    <li><a >News</a></li>
                                    <li><a >About Us</a></li>
                                    <li><Link to={'login'}>Sign In</Link></li>
                                    <li><a >Create an Account</a></li>    
                                </ul>
                            </div>
                        </div>
                        </nav>
                    </header>
                }
            </div>
        )
    }
}

export default Header;