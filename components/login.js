import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import Loader from 'react-loader-spinner';
import { NodeAPI } from '../services/webServices';
import { getUserDetail } from '../services/global';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: '',
      password: '',
      emailErrorMsg: '',
      passwordErrorMsg: '',
      emailDisabled: false,
      passDisabled: false,
      rememberMe:false,
      disabled: true
    }
    this.loginClick = this.loginClick.bind(this);
  }

  componentDidMount(){
    if(localStorage.getItem('rememberMe') != null){
      let rememberStatus = JSON.parse(localStorage.getItem('rememberMe'));
      if(rememberStatus.remember){
        this.setState({email: rememberStatus.email, password: rememberStatus.password, rememberMe:rememberStatus.remember, disabled: false});
      }
    }
  }

  // Handler to disabled particular field which does not meet the validation
  checkDisabled() {
    let value = this.state;
    if(value.emailDisabled && value.passDisabled) {
      this.setState({disabled: false});
    }
  }

  // Handle user input fields to update their states
  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    let emailReg = /^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i;
    if (name == "email") {
      if(value == ''){
        this.setState({
          emailErrorMsg: 'Email mandatory',
          [name]: '',
          emailDisabled: false
        })
      } else if(!emailReg.test(value)) {
        this.setState({
          emailErrorMsg: 'Not a valid Email',
          [name]: value,
          emailDisabled: false
        })
      } else {
        this.setState({
          emailErrorMsg: '', 
          [name]: value,
          emailDisabled: true 
        })
      }
    } else if (name == "password") {
      if(value == ''){
        this.setState({
          passwordErrorMsg: 'Password mandatory',
          [name]: '',
          passDisabled: false
        })
      } else {
        this.setState({
          passwordErrorMsg: '', 
          [name]: value,
          passDisabled: true 
        })
      }
    }
  }

  // Remember me functionality
  handleRemMe(e){
    this.setState({rememberMe: e.target.value=='true'?false:true});
  }

  // API to login the user
  loginClick(e) {
    e.preventDefault();
    this.setState({loading: true});
    let loginData = {
      "email": this.state.email,
      "password": this.state.password
    }
    return NodeAPI(loginData, "/login_user", "POST")
      .then(responseJson => {
        this.setState({loading: false});
        if(responseJson.responseCode == 200) {
          localStorage.setItem('isLogged',this.state.rememberMe);
          localStorage.setItem('rememberMe',JSON.stringify({remember:this.state.rememberMe, email:this.state.email,password:this.state.password}));
          localStorage.setItem('user',JSON.stringify(responseJson.data));
          getUserDetail();
          this.props.history.push('/authentication');
        }else {
          toast.error(responseJson.responseMessage, {
            position: toast.POSITION.TOP_RIGHT
          });
        }
      })
      .catch(error => { this.setState({loading: false}); }) 
  }

  render() {
    var bgImg=require('../assets/images/bg-01.jpg');
    return (
      <div className="App">
        {this.state.loading ? <div className="loading_outer"><div className="loading_inner"><Loader type="Circles" color="#00BFFF" height="120" width="120" /></div></div> : null}
        <ToastContainer autoClose={3000} />
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <div className="login100-form-title" style={{backgroundImage: "url("+bgImg+")"}}>
                <span className="login100-form-title-1">
                  Login
                </span>
              </div>
              <form className="login100-form validate-form">
                <div className="wrap-input100 validate-input mb25">
                  <span className="label-input100">Email</span>
                  <input type="email" className="form-control" name="email" value={this.state.email} placeholder="Email ID" onChange={(event) => {this.handleUserInput(event);this.checkDisabled();}} />
                  <span className="focus-input100"></span>
                </div>
                <span _ngcontent-c4="" className="form-valid-err-msg" style={{color:'red', marginBottom: 14+'px', marginTop: -19+'px'}}>{this.state.emailErrorMsg}</span>
                <div className="wrap-input100 validate-input mb25">
                  <span className="label-input100">Password</span>
                  <input type="password" className="form-control" name="password" value={this.state.password} placeholder="Password" onChange={(event) => {this.handleUserInput(event); this.checkDisabled();}} />
                  <span className="focus-input100"></span>
                </div>
                <span _ngcontent-c4="" className="form-valid-err-msg" style={{color:'red', marginBottom: 14+'px', marginTop: -19+'px'}}>{this.state.passwordErrorMsg}</span>
                <div className="flex-sb-m w-full pb30">
                  <div className="contact100-form-checkbox">
                    <input className="input-checkbox100" id="ckb1" type="checkbox" name="rememberMe" value={this.state.rememberMe} checked={this.state.rememberMe} onChange={(event) => {this.handleRemMe(event);}} />
                    <label className="label-checkbox100" htmlFor="ckb1">
                      Remember me
                    </label>
                  </div>
                  <div>
                    <a className="txt1">
                      Forgot Password?
                    </a>
                  </div>
                </div>
                <div className="container-login100-form-btn">
                  <button className="login100-form-btn" onClick={this.loginClick} disabled={this.state.disabled}>
                    Login {this.state.emailErrorMsg}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;