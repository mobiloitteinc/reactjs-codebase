import React, { Component } from 'react';
import Header from './header';
import QRCode from 'qrcode.react';
import { socket, initEvent } from '../services/socket';
import { userDetail, getUserDetail } from '../services/global';

class Wallet extends Component {

  constructor(props){
    super(props);
    this.state = {
      user_detail: userDetail,
      qrCodeVal: {name:'', total_price:0, address:''}
    }
    this.logout = this.logout.bind(this);
  }

  componentDidMount(){
    // user data socket listen
    socket.on('userData', (data) => {
      localStorage.setItem('user',JSON.stringify(data));
      getUserDetail();
      this.setState({user_detail: JSON.parse(localStorage.getItem('user'))});
    });
  }

  // Logout handler
  logout() {
    this.props.history.push('/');
    this.setState({
      user_detail:null
    });
  }

  // Hander for open modal for respective coins
  openAddressModal(e, data){
    e.preventDefault();
    this.state.qrCodeVal = data;
    this.forceUpdate();
  }

  render() {
    return (
      <div className="wrapper">
        <div id="qrcode_address_modal" className="modal fade global-modal2" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header model-header-bdr clearfix" style={{border: 'none'}}>
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><span>×</span></button>
              </div>
              <div className="modal-body">
                <div className="qr-modal-msg">
                  <label id="coinMessage">Your {this.state.qrCodeVal.name} Address:</label>
                  <p id="walletAddress">{this.state.qrCodeVal.address}</p>
                  <label id="qrMessage">Your {this.state.qrCodeVal.name} QR Code:</label>
                  <div className="qr-code-img">
                    <QRCode value={this.state.qrCodeVal.address} size={128} level="M" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Header logoutHandler={ this.logout } />
        <div className="exchange-wrapper">
          <section className="wallet-section common-section">
            <div className="common-container">
              <div className="current-balance-block">
                <div className="row">
                  <div className="col-sm-7">
                    <div className="show-balance">
                      <img src={require('../assets/images/current-icon.png')} />
                      <span>Current Balance:</span>
                      <span id="depBal">{this.state.user_detail['BTC'].total_price}</span> BTC
                    </div>
                  </div>
                  <div className="col-sm-5 add-money-btn">
                    <a className="btn btn-green-bg" id="addMoneyLink">Add Money</a>
                  </div>
                </div>
              </div>
              <div className="global-table">
                <div className="table-responsive">
                  <div id="wallet_wrapper" className="dataTables_wrapper form-inline dt-material no-footer">
                    <div className="mdl-grid dt-table">
                      <div className="mdl-cell mdl-cell--12-col">
                        <table className="table table-bordered mdl-data-table wallet-table dataTable no-footer" id="wallet" role="grid" aria-describedby="wallet_info">
                          <thead>
                            <tr role="row">
                              <th className="sorting_asc" aria-controls="wallet" rowSpan="1" colSpan="1" aria-sort="ascending" aria-label="CURRENCY NAME: activate to sort column descending">CURRENCY NAME</th>
                              <th className="sorting" aria-controls="wallet" rowSpan="1" colSpan="1" aria-label="SYMBOL: activate to sort column ascending">SYMBOL</th>
                              <th className="sorting" aria-controls="wallet" rowSpan="1" colSpan="1" aria-label="AVAILABLE BALANCE: activate to sort column ascending">AVAILABLE BALANCE</th>
                              <th className="sorting" aria-controls="wallet" rowSpan="1" colSpan="1" aria-label="WALLET ADDRESS: activate to sort column ascending">WALLET ADDRESS</th>
                              <th className="sorting" aria-controls="wallet" rowSpan="1" colSpan="1" aria-label="ACTION: activate to sort column ascending">ACTION</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr className="active odd" role="row">
                              <td className="currencyName sorting_1">Bitcoin</td>
                              <td>BTC</td>
                              <td>{this.state.user_detail['BTC'].total_price}</td>
                              <td className="walletAddress">rttyrtyrtyrtyrtyrty</td>
                              <td>
                                <button className="btn btn-orange send" data-toggle="modal" data-target="#qrcode_address_modal" onClick={(event) => {this.openAddressModal(event, {name:'Bitcoin', total_price: this.state.user_detail['BTC'].total_price, address: 'hadjagdjhajdhajgdh'});}}>Deposit</button>
                                <button className="btn btn-sky-blue receive" data-toggle="modal" data-target="#qrcode_address_modal" onClick={(event) => {this.openAddressModal(event, {name:'Bitcoin', total_price: this.state.user_detail['BTC'].total_price, address: 'dfsgsgdfggfd'});}}>Withdraw</button>
                              </td>
                            </tr>
                            <tr className="active even" role="row">
                              <td className="currencyName sorting_1">Ethereum</td>
                              <td>ETH</td>
                              <td>{this.state.user_detail['ETH'].total_price}</td>
                              <td className="walletAddress">dfggjfghj56756756756</td>
                              <td>
                                <button className="btn btn-orange send" data-toggle="modal" data-target="#qrcode_address_modal" onClick={(event) => {this.openAddressModal(event, {name:'Ethereum', total_price: this.state.user_detail['ETH'].total_price, address: 'sdfsdfsdf'});}}>Deposit</button> 
                                <button className="btn btn-sky-blue receive" data-toggle="modal" data-target="#qrcode_address_modal" onClick={(event) => {this.openAddressModal(event, {name:'Ethereum', total_price: this.state.user_detail['ETH'].total_price, address: 'sdfsdfsdfsdf'});}}>Withdraw</button>
                              </td>
                            </tr>
                            <tr className="active even" role="row">
                              <td className="currencyName sorting_1">Litecoin</td>
                              <td>LTC</td>
                              <td>{this.state.user_detail['LTC'].total_price}</td>
                              <td className="walletAddress">56456ghjfghjfhjhgj6756</td>
                              <td>
                                <button className="btn btn-orange send" data-toggle="modal" data-target="#qrcode_address_modal" onClick={(event) => {this.openAddressModal(event, {name:'Litecoin', total_price: this.state.user_detail['LTC'].total_price, address: 'sdfsdfsdfsdf'});}}>Deposit</button> 
                                <button className="btn btn-sky-blue receive" data-toggle="modal" data-target="#qrcode_address_modal" onClick={(event) => {this.openAddressModal(event, {name:'Litecoin', total_price: this.state.user_detail['LTC'].total_price, address: 'fgdfgdfgdfg'});}}>Withdraw</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
export default Wallet;