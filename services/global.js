let  userDetail = null;

function getUserDetail(){
    userDetail = JSON.parse(localStorage.getItem('user'));
}

function logout(){
    userDetail = null;
    localStorage.removeItem('isLogged');
    localStorage.removeItem('user');
}

export { getUserDetail, userDetail, logout };