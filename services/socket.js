import openSocket from 'socket.io-client';
 const  socket = openSocket('http://172.26.36.234:4055');

function initSocket(){
    socket.on('connect', () => {
        console.log('Socket is connected');
        initEvent('LTC');
    });
}

function initEvent(coin_type){
    if(localStorage.getItem('isLogged') == null){
        socket.emit('startExchange',{coin_type:coin_type, user_id:null});
    }else{
        let user_id = JSON.parse(localStorage.getItem('user')).user_id;
        socket.emit('startExchange',{coin_type:coin_type, user_id:user_id});
    }
}

export { initSocket, socket, initEvent };