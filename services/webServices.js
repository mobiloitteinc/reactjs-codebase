
function NodeAPI(variables, apiName, apiMethod, token){
    const baseUrl = "http://172.26.36.234:4055";

    var init = apiMethod == "GET" ? {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
    } :
        {
            method: apiMethod,
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify(variables)
        }
    if(token)
        init.headers.Authorization = token;
    return fetch(baseUrl + apiName, init)
        .then(response => response.json()
            .then(responseData => {
                return responseData;
            }))
        .catch(err => {
            return { responseMessage: "Something went wrong. Please try again." }
        });
}

function thirdPartyAPI(variables, url, apiMethod){
    var init = apiMethod == "GET" ? {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    } :
        {
            method: apiMethod,
            headers: {
                'Content-Type': "application/json",
            },
            body: JSON.stringify(variables)
        }

    return fetch(url, init)
        .then(response => response.json()
            .then(responseData => {
                return responseData;
            }))
        .catch(err => {
            return { responseMessage: "Something went wrong. Please try again." }
        });
}

export { NodeAPI, thirdPartyAPI };